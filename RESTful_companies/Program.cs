﻿using System;
using System.Web.Http;
using System.Web.Http.SelfHost;

namespace RESTful_companies
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new HttpSelfHostConfiguration("http://localhost:8080");
            config.Routes.MapHttpRoute(
                "Action API", "api/{controller}/{action}/{id}",
                new { id = RouteParameter.Optional });
            config.Filters.Add(new BasicAuthenticationAttribute());

            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            {
                server.OpenAsync().Wait();
                Console.WriteLine("Press any key to quit.");
                Console.ReadKey();
            }
        }
    }
}
