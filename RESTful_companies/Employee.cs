﻿using System;

namespace RESTful_companies
{
    public enum JobEnum
    {
        Administrator,
        Developer, 
        Architect,
        Manager
    }

    public class Employee
    {
        public virtual long Id { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime DateOfBirth { get; set; }
        public virtual JobEnum JobTitle { get; set; }
    }
}
