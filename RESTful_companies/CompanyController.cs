﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RESTful_companies
{
    public struct SearchQuery
    {
        public string Keyword;
        public DateTime EmployeeDateOfBirthFrom;
        public DateTime EmployeeDateOfBirthTo;
        public List<JobEnum> EmployeeJobTitles;
    }

    [BasicAuthentication]
    public class CompanyController : ApiController
    {
        private ISessionFactory mySessionFactory;

        public CompanyController()
        {
            mySessionFactory = new Configuration().Configure().BuildSessionFactory();
        }

        // POST: /company/create
        [HttpPost]
        [ActionName("create")]
        public IHttpActionResult CreateCompany(Company c)
        {
            if (Object.Equals(c, default(Company)))
                return BadRequest(); // company is not initialized, 400

            long idOfCreation;
            using (ISession mySession = mySessionFactory.OpenSession())
            using (mySession.BeginTransaction())
            {
                mySession.Save(c);
                idOfCreation = (long)mySession.GetIdentifier(c);
                mySession.Transaction.Commit();
            }
            return Ok(idOfCreation); // 200
        }

        // POST: /company/search
        [HttpPost]
        [OverrideAuthorization]
        [ActionName("search")]
        public HttpResponseMessage SearchCompany(SearchQuery sq)
        {
            if (sq.EmployeeDateOfBirthTo.Year < sq.EmployeeDateOfBirthFrom.Year)
                return Request.CreateResponse<IEnumerable<Company>>(HttpStatusCode.BadRequest, null);

            ISession mySession = mySessionFactory.OpenSession();
            using (mySession.BeginTransaction())
            {
                ICriteria requestedCompaniesCriteria = mySession.CreateCriteria<Company>("company")
                    .CreateAlias("Employees", "employee");

                if (sq.Keyword != null && sq.Keyword != "") // keyword given
                    requestedCompaniesCriteria
                       .Add(Restrictions.Disjunction()
                           .Add(Restrictions.Like("company.Name", sq.Keyword, MatchMode.Anywhere))
                           .Add(Restrictions.Like("employee.FirstName", sq.Keyword, MatchMode.Anywhere))
                           .Add(Restrictions.Like("employee.LastName", sq.Keyword, MatchMode.Anywhere)));

                if (sq.EmployeeDateOfBirthFrom.Year > 1900) // date of birth given
                    requestedCompaniesCriteria.Add(
                        Restrictions.Between("employee.DateOfBirth", sq.EmployeeDateOfBirthFrom, sq.EmployeeDateOfBirthTo));

                if (sq.EmployeeJobTitles.Count > 0) // job titles given
                    requestedCompaniesCriteria
                           .Add(Restrictions.In("employee.JobTitle", sq.EmployeeJobTitles));

                // remove duplicated companies by distinct projection
                requestedCompaniesCriteria.SetProjection(
                    Projections.Distinct(Projections.Entity<Company>("company")));

                IList<Company> requestedCompaniesList = requestedCompaniesCriteria.List<Company>();
                return Request.CreateResponse<IEnumerable<Company>>(HttpStatusCode.OK, requestedCompaniesList); // returns 200
            }
        }

        // PUT: /company/update/<id>
        [HttpPut]
        [ActionName("update")]
        public IHttpActionResult UpdateCompany(Company c, long id)
        {
            using (ISession mySession = mySessionFactory.OpenSession())
            using (mySession.BeginTransaction())
            {
                try
                {
                    mySession.Update(c, id);
                    mySession.GetCurrentTransaction().Commit();
                    return Ok(id);
                }
                catch
                {
                    return NotFound(); // 404
                }
            }
        }

        // DELETE: /company/delete/<id>
        [HttpDelete]
        [ActionName("delete")]
        public IHttpActionResult DeleteCompany(long id)
        {
            using (ISession mySession = mySessionFactory.OpenSession())
            using (mySession.BeginTransaction())
            {
                try
                {
                    var company = mySession.Get<Company>(id);
                    mySession.Delete(company);
                    mySession.GetCurrentTransaction().Commit();
                    return Ok(id);
                }
                catch
                {
                    return NotFound();
                }
            }
        }
    }
}
