﻿using System.Collections.Generic;

namespace RESTful_companies
{
    public class Company
    {
        public virtual long Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int EstablishmentYear { get; set; }
        public virtual ISet<Employee> Employees { get; set; }
    }
}
