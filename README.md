# RESTful companies

C# application (.NET Framework) that handles storage of informations about companies and their employees in MS SQL database. Management is done through RESTful Web API and includes actions of creation, deletion, edition and search by keywords. 

The application is based on System.Web.Http.SelfHost library.

Preconfigured to listen on port 8080 with michal:demo basic authorization for certain actions. Supports HTTP methods listed below:

****

**POST** _/api/company/create_

Creates company in database and returns id generated. List can be empty. _Needs authorization._
```
{
    "Name": "<string>",
    "EstablishmentYear”: <integer>,
    "Employees": [{
        "FirstName": "<string>",
        "LastName": "<string>",
        "DateOfBirth": "<DateTime>",
        "JobTitle": "<int>"
    }, ...]
}
```
reply:
```
{
    "Id”: <long>
}
```

****

 **POST**: _/api/company/search_

Returns list of entries that fulfill criteria. Keyword works on either any employee first name or last name, or company name. There is conjuction between keyword, employee date of birth range and employee job titles, meaning that given all, company must contain keyword, employees in chosen date of birth range and employees with given job titles. Any field can be null.  _Doesn't need authorization._
```
{
    "Keyword": "<string>",
    "EmployeeDateOfBirthFrom": "<DateTime?>",
    "EmployeeDateOfBirthTo": "<DateTime?>",
    "EmployeeJobTitles": [“<int>, ...]
}
```
reply:
```
[{
    "Id": "<long>",
    "EstablishmentYear”: <integer>,
    "Name": "<string>",
    "Employees": [{
        "FirstName": "<string>",
        "LastName": "<string>",
        "DateOfBirth": "<DateTime>",
        "JobTitle": "<int>"
    }, ...]
}, ...]
```

****
**PUT** _/api/company/update/<id>_

Updates given object with data. _Needs authorization._
```
{
    "Name": "<string>",
    "EstablishmentYear”: <integer>,
    "Employees": [{
        "FirstName": "<string>",
        "LastName": "<string>",
        "DateOfBirth": "<DateTime>",
        "JobTitle": "<int>"
    }, ...]
}
```

****

**DELETE** _/api/company/delete/<id>_

Deletes object with given id from the database. _Needs authorization._

